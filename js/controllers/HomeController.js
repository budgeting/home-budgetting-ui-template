var MONTH_SALARY = 4180;
var BI_SALARY = MONTH_SALARY/2;

app.controller('homeController', function($scope, BillService, $log, BankAccountService) {
    $scope.loading = true;
    init();
    
    function init() {        
        $scope.bills1 = BillService.getFirstHalf();
        $scope.bills2 = BillService.getSecondHalf();
        $scope.bankAccounts = BankAccountService.getBankAccs();
        
        $scope.loading = false;
        
        initMoneyDistribution();
    }
    
    $scope.setAccount = function(account) {
        $scope.bankAcc = account;   
    }
    
    $scope.goUp = function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
    
    
    function initMoneyDistribution() {
        $scope.firstHalfBudget = createBudget($scope.bills1);
        $scope.secondHalfBudget = createBudget($scope.bills2);
        
    }

    function createBudget(bills) {
        var budget = initBudget();
        
        if(hasItems(bills)) {
            var reoccuringBills = getAccountPayments(bills[0].accounts);
            var debt = isDefined(bills[1]) ? getAccountPayments(bills[1].accounts) : 0;  
            
            var totalExpenses = reoccuringBills + debt;
            var remainingSalary = BI_SALARY - totalExpenses;
        
            budget = {
                funMoney: (remainingSalary * .25),
                expenseAllowence: (remainingSalary * .5),
                savings: (remainingSalary * .25),
                debt: debt,
                reoccuringBills: reoccuringBills,
                totalExpenses: totalExpenses
            };
            
        }
        
        return budget;
    }
    
    function initBudget() {
        
        return {
            funMoney: 0,
            expenseAllowence: 0,
            savings: 0,
            debt: 0,
            reoccuringBills: 0
        };
    
    }
    
    
    function getAccountPayments(accs) {
        var amount = 0;
        if(hasItems(accs)) {
            
            for(var i = 0; i < accs.length; i++) {
                amount += accs[i].amount;   
            }
        }
        
        return amount;
    }
        
    function hasItems(list) {
        return isDefined(list) && isDefined(list.length) && list.length > 0;   
    }
        
    function isDefined(obj) {
        return obj !== null && obj !== undefined;   
    }
    
    
    


});