app.factory('BillService', function($http, $log) {
    
   return {
        getFirstHalf: function() {
            return [
               {
                   name: "Reoccurring Bills",
                   info: "reoccuring bills for the first part of the month",
           
                   accounts: [
                {
                    name: 'Geico Insurance',
                    url: 'https://google.com',
                    userName: 'someUserName',
                    paid: false,
                    password: 'somePassword',
                    amount: 158.91
                },
                {
                    name: 'Rent',
                    url: 'https://google.com',
                    userName: 'userName',
                    paid: true,
                    password: 'password',
                    amount: 1000
                } 
                   ]
               },
               {
                   name: "Outstanding Debt",
                   info: "outstanding debt for the first part of the month",
               
                   accounts: [
                {
                    name: 'Florida Loan Amount',
                    userName: 'N/A',
                    password: 'N/A',
                    paid: true,
                    amount: 125
                }
                   ]
               }
            ];
        },
       
         getSecondHalf: function() {
            return [
               {
                   name: "Reoccurring Bills",
                   info: "reoccuring bills for the second part of the month.",
                   accounts: [
                {
                    name: 'Gas',
                    url: 'https://google.com',
                    userName: 'userName',
                    password: 'password',
                    amount: 50,
                    paid: false
                }
                   ]
               },
               {
                    name: "Outstanding Debt",
                    info: "oustanding debt for the second part of the month",
                
                   accounts: [
                {
                    name: 'Discover Card',
                    url: '',
                    userName: 'userName',
                    password: 'password',
                    paid: false,
                    amount: 125
                }
                   ]
               }
            ];
        }      
       
  
   };
});