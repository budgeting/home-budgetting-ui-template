app.factory('BankAccountService', function($http, $log) {
    var genericDescription = 'Check, credit, and debit information regarding this bank';
    
    var floridaAccount = {
        displayName: 'CCU of Florida',
        description: genericDescription,
        userName: 'myUserName',
        password: 'password',
        debitCard: createCard('David Turner', 5555555555555, '03/20', 111, 'Visa'),
        creditCard: createCard('David S Turner', 55555555555, '03/20', 111, 'Visa'),
        checkInfo: createCheckInfo(55555555, 555555555)
    };

    var ccuAccount = {
        displayName: 'Communit America Credit Union',
        description: genericDescription,
        userName: 'myUserName',
        password: 'myPassword!',
        debitCard: createCard('David S Turner', 55555555555, '02/20', 111, 'Visa'),
        checkInfo: createCheckInfo(555555555, 555555555555)
    };

    var academyAccount = {
        displayName: 'Academy Bank',
        userName: 'N/A',
        password: 'N/A',
        description: genericDescription,
        debitCard: createCard('David Turner', 555555555, '03/20', 111, 'Visa'),    
    };

    function createCard(name, number, exp, cvv, cardType) {
        return {
            name: name,
            number: number,
            exp: exp,
            cvv: cvv,
            cardType: cardType,
        };
    }

    function createCheckInfo(routingNum, accNum) {
        return {
            routingNumber: routingNum,
            accountNumber: accNum
        };
    }
    return {
        
        getBankAccs: function() {
            return [
                floridaAccount, ccuAccount, academyAccount
            ]
        },
        
        getFloridaAcc: function() {
            return floridaAccount;
        },
            
        getAcademyAcc: function() {
            return academyAccount;
        },
            
        getCcuAcc: function() {
            return ccuAccount;
        }
        
    };
    
    
});